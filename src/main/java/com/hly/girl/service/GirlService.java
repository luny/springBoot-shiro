package com.hly.girl.service;

import com.hly.girl.dao.GirlRepository;
import com.hly.girl.pojo.Girl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 *@Author 黄露莹
 *@Date 2017/10/18/018 17:16
 */
@Service
public class GirlService {
    @Autowired
    private GirlRepository girlRepository;

    @Transactional
    public void insertTwo(){
        Girl girlA = new Girl();
        girlA.setSize("C");
        girlA.setAge(17);
        girlRepository.save(girlA);

        Girl girlB= new Girl();
        girlB.setSize("CAAAAAAA");
        girlB.setAge(12);
        girlRepository.save(girlB);
    }


   /*这个是Mybait的插件*/
   /* @org.springframework.transaction.annotation.Transactional(propagation = Propagation.SUPPORTS)
    public List<Girl> queryUserListPaged(Girl user, Integer page, Integer pageSize) {
        // 开始分页
        PageHelper.startPage(page, pageSize);
        List<Girl> userList = girlRepository.findAll();

        return userList;
    }*/
}

package com.hly.girl.Interceptor;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ExceptionHaddler {
    public static final String ERROR_NAME="error";
    @ExceptionHandler(value = Exception.class)
    public Object errorMeth(HttpServletRequest request, HttpServletResponse response, Exception e) throws Exception {
        e.printStackTrace();

        ModelAndView v = new ModelAndView();
        v.addObject("exception",e);
        v.addObject("url",request.getRequestURL());
        v.setViewName(ERROR_NAME);
        return v;
    }
}

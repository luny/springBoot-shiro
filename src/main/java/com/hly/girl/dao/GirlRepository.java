package com.hly.girl.dao;

import com.hly.girl.pojo.Girl;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 *@Author 黄露莹
 *@Date 2017/10/18/018 16:31
 */
public interface GirlRepository extends JpaRepository<Girl,Integer> {//实体名，id的类型
    //通过年龄查询
    public List<Girl> findByAge(Integer age);//命名要规范哦
}

package com.hly.girl.dao;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 *@Author 黄露莹
 *@Date 2017/10/13/013 14:24
 */

@Component
@ConfigurationProperties(prefix = "girl")
public class GirlProperties {
    private String size;
    private Integer age;


    public String getSize() {
        return size;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
                this.age = age;
    }

    public void setSize(String size) {
        this.size = size;
    }
}

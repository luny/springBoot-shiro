package com.hly.girl.controller;

import com.hly.girl.dao.GirlRepository;
import com.hly.girl.pojo.Girl;
import com.hly.girl.service.GirlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *@Author 黄露莹
 *@Date 2017/10/18/018 16:29
 */
@RestController
@RequestMapping(value = "girlController")
public class GirlController {
    @Autowired
    private GirlRepository girlRepository;
    @Autowired
    private GirlService girlService;
    /**
     * 获取列表   /girlController/girls
     * @return
     */
    @GetMapping(value = "/girls")
    public List<Girl> girlList(){
        return  girlRepository.findAll();
    }

    /**
     * 添加
     * @param size
     * @param age
     * @return
     */
    @PostMapping(value = "girlAdd")
    public Girl girlAdd(@RequestParam("size") String size,
                        @RequestParam("age") Integer age){
        Girl girl = new Girl();
        girl.setAge(age);
        girl.setSize(size);
        return girlRepository.save(girl);
    }

    /**
     * 通过id获取数据
     * @return
     */
    @GetMapping(value = "/girl/{id}")
    public Girl getById(@PathVariable("id") Integer id){
        return girlRepository.getOne(id);
    }
    /**
     * 通过age获取数据
     * @return
     */
    @GetMapping(value = "/girl/age/{age}")
    public List<Girl> girlListByAge(@PathVariable("age") Integer age){
        return girlRepository.findByAge(age);
    }
    /**
     * 更新
     * @return
     */
    @PutMapping(value = "/girl/{id}")
    public Girl girlUpdate(@PathVariable("id") Integer id,
                           @RequestParam("size") String size,
                           @RequestParam("age") Integer age){
        Girl girl = new Girl();
        girl.setSize(size);
        girl.setAge(age);
        girl.setId(id);
        return girlRepository.save(girl);
    }

    /**
     * 删除
     */
    @DeleteMapping(value = "/girl/{id}")
    public void girlDel(@PathVariable("id") Integer id){
        girlRepository.delete(id);
    }
    @PutMapping(value = "/girl/two")
    public void girlTwo(){
        girlService.insertTwo();
    }

   /* @RequestMapping("/queryPage")
    public List<Girl> queryPage(Integer page) {

        if (page == null) {
            page = 1;
        }

        int pageSize = 1;

        Girl girl = new Girl();
//		user.setNickname("lee");

        List<Girl> list = girlService.queryUserListPaged(girl, page, pageSize);

        return list;
    }*/
}

package com.hly.girl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "errorController")
public class ErrorController {
    @RequestMapping(value = "/err")
    public String err(){
        int i = 1/0;
        return "error";
    }
}

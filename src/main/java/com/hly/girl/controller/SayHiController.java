package com.hly.girl.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SayHiController {
    @RequestMapping(value = "/hi", method = RequestMethod.GET)
    public String sayN() {
        return "index";
    }
}

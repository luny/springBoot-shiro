package com.hly.girl;

import com.hly.girl.dao.GirlProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

/**
 *@Author 黄露莹
 *@Date 2017/10/12/012 17:24
 */

@RestController
@RequestMapping(value = "helloController")
public class HelloController1 {

    @Value("${size}")
    private String size;

    @Value("${age}")
    private Integer age;

    @Value("${content}")
    private String content;
    @Autowired
    private GirlProperties girlProperties;

    @RequestMapping(value = "/hello",method = RequestMethod.GET)
    public String say(){
        return content;
    }
    @RequestMapping(value = {"/hello1","hi1"},method = RequestMethod.GET)
    public String say1(){
        return girlProperties.getSize();
    }

    /**
     * 获取参数,参数位置可自定义，放在前后都可以
     * URL格式：http://127.0.0.1:8081/girl/helloController/say2/100
     * @return id: 100
     */
    @RequestMapping(value = "/say2/{id}",method = RequestMethod.GET)
    public String say2(@PathVariable("id") Integer id){
        return "id: "+id;
    }

    /**
     * 获取参数2   RequestParam
     * 注意：如果不设置不必填和默认值，不传参数时会报错
     * URL格式：http://127.0.0.1:8081/girl/helloController/say3?id=100
     * @return id: 100
     */
    @RequestMapping(value = "/say3",method = RequestMethod.GET)
    public String say3(@RequestParam(value = "id",required = false,defaultValue = "0") Integer myId){
        return "id: "+myId;
    }
    @GetMapping(value = "/getMapping")
    public String getMapping(){
        return "getMapping";
    }


}

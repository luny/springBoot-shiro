package com.hly.shiro;

import com.hly.girl.shiro.MyRealm;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

public class MyRealmTest {

    MyRealm myRealm = new MyRealm();


    @Test
    public void testAuthentication(){
        //1、构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager =  new DefaultSecurityManager();
        defaultSecurityManager.setRealm(myRealm);//强ream设置到环境中

       //md5加密
        HashedCredentialsMatcher matcher = new HashedCredentialsMatcher();
        matcher.setHashAlgorithmName("md5");//算法名称
        matcher.setHashIterations(1);//加密次数
        myRealm.setCredentialsMatcher(matcher);

        //2、主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("hly","123");
        try{
            //登录成功
            subject.login(token);//登录
        }catch (Exception e){
            //登陆失败
        }
        System.out.println("isAuthenticated："+subject.isAuthenticated());
      /*  subject.checkRole("admin");//校验角色
        subject.checkRoles("admin","user");//校验角色--多角色*/
/*
        if(subject.hasRole("admin")){
            System.out.println("拥有管理员权限");
        }else{
            System.out.println("不是管理员");
        }*/
        subject.logout();//退出
        System.out.println("isAuthenticated："+subject.isAuthenticated());

    }
}

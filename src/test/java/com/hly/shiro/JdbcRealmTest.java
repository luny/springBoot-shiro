package com.hly.shiro;

import com.alibaba.druid.pool.DruidDataSource;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

public class JdbcRealmTest {

   DruidDataSource ds = new DruidDataSource();
    {
        //ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl("jdbc:mysql://127.0.0.1:3306/dbgirl");
        ds.setUsername("root");
        ds.setPassword("ying1527");
    }

    @Test
    public void testAuthentication(){
        JdbcRealm jdbcRealm = new JdbcRealm();
        jdbcRealm.setDataSource(ds);
        jdbcRealm.setPermissionsLookupEnabled(true);

        //查询用户密码
        String sql = "select PASSWORD from test_user where username = ?";
        jdbcRealm.setAuthenticationQuery(sql);
        //查询权限
        String sql1 = "select role_name from test_user_roles where username = ?";
        jdbcRealm.setUserRolesQuery(sql1);

        //1、构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager =  new DefaultSecurityManager();
       // defaultSecurityManager.setRealm(simpleAccountRealm);//强ream设置到环境中
        defaultSecurityManager.setRealm(jdbcRealm);

        //2、主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("hly","123");
        try{
            //登录成功
            subject.login(token);//登录
            System.out.println("isAuthenticated："+subject.isAuthenticated());
        }catch (Exception e){
            //登陆失败
        }
        /*System.out.println("isAuthenticated："+subject.isAuthenticated());
        subject.checkRole("admin");//校验角色
        subject.checkRoles("admin","user");//校验角色--多角色

        if(subject.hasRole("admin")){
            System.out.println("拥有管理员权限");
        }else{
            System.out.println("不是管理员");
        }*/
        subject.logout();//退出


    }
}

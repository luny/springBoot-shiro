package com.hly.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.text.IniRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Test;

public class IniRealmTest {
    @Test
    public void testAuthentication(){

        IniRealm iniRealm = new IniRealm("classpath:user.ini");

        //1、构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager =  new DefaultSecurityManager();
        defaultSecurityManager.setRealm(iniRealm);
        //2、主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("hly","123");
        try{
            //登录成功
            subject.login(token);//登录
        }catch (Exception e){
            //登陆失败
        }

        System.out.println("isAuthenticated："+subject.isAuthenticated());
        if(subject.hasRole("admin")){
            System.out.println("拥有管理员权限");
            //是否拥有这个权限
            subject.isPermitted("user:delete");//返回Boolean
            //是否拥有一下所有的权限
            subject.isPermittedAll("user:delete","user:update");//返回boolean
            subject.checkPermission("user:delete");//是否拥有user:delete权限--正确继续，错误跑出异常
            subject.checkPermissions("user:delete");//是否拥有user:delete权限--正确继续，错误跑出异常


        }else{
            System.out.println("不是管理员");
        }
    }
}

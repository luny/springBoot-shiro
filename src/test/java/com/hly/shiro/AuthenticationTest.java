package com.hly.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.realm.SimpleAccountRealm;
import org.apache.shiro.subject.Subject;
import org.junit.Before;
import org.junit.Test;

public class AuthenticationTest {

    SimpleAccountRealm simpleAccountRealm = new SimpleAccountRealm();
    @Before
    public  void addUser(){
        simpleAccountRealm.addAccount("hly","123","admin","user","user1");
    }

    @Test
    public void testAuthentication(){
        //1、构建SecurityManager环境
        DefaultSecurityManager defaultSecurityManager =  new DefaultSecurityManager();
        defaultSecurityManager.setRealm(simpleAccountRealm);//强ream设置到环境中
        //2、主体提交认证请求
        SecurityUtils.setSecurityManager(defaultSecurityManager);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken("hly","123");
        try{
            //登录成功
            subject.login(token);//登录
        }catch (Exception e){
            //登陆失败
        }
        System.out.println("isAuthenticated："+subject.isAuthenticated());
        subject.checkRole("admin");//校验角色
        subject.checkRoles("admin","user");//校验角色--多角色

        if(subject.hasRole("admin")){
            System.out.println("拥有管理员权限");
        }else{
            System.out.println("不是管理员");
        }
        subject.logout();//退出
        System.out.println("isAuthenticated："+subject.isAuthenticated());

    }
}
